---
title: 自然语言处理介绍
date: 2018-08-20 22:15:09
description: 自然语言处理（NLP）是计算机科学，人工智能，语言学关注计算机和人类（自然）语言之间的相互作用的领域。
categories: 
    - Algorithm
tags: 
    - Science
    - Intelligence
    - Linguistics
---
###  What is NLP

NLP = NLU + NLG

​	NLU：语音/文本 ---- 意思 （meaning）

​	NLG：意思 ---- 文本/语音

### Why NLP is Harder than Computer Vision

​	图片：所见即所得

​	文本：Ambiguity（一词多义）

### NLP的四个维度

​	Semantic ( 语义 ) ----NLU (理解语义)、 

​	Syntax ( 句子结构 ) ----  句法分析（每种语言都有每种的句法，词性等）、依存分析（单词单词之间关系）

​	Morphology ( 单词 ) ---- 分词、pos、ner...

​	Phonetics ( 声音 )

### NLP的三大类

#### 	Easy or mostly solved	//简单或者基本解决的

​		Named Entity Recognition	// 命名实体识别

​		Participles							 // 分词

​		Spam Detection				    // 垃圾邮件分类

​		Part of Speech Tagging	    // 词性标注

​		...

#### 	Intermediate or making good progress		//中等或者有一定进展的

​		Sentiment analysis			  	// 情感分析

​		Coreference resolution			// 算法

​		Word sense disambiguation	// 词义消歧

​		Syntactic analysis					// 句法分析

​		Machine Translation				// 机器翻译

​		Information Translation			// 信息翻译

​		...

#### 	Hard or still need lot of work		//困难或者还需要一定时间的

​		Text Summarization				// 文章摘要（生成）

​		Machine Dialog System		 // 机器对话系统

​		...

## Pipeline

| 原始文本 | 分词 | 清洗                | 标准化         | 特征提取    | 建模       |
| -------- | ---- | ------------------- | -------------- | ----------- | ---------- |
| 网页文本 | Jibe | 无用的标签&特殊符号 | ate ---- eat   | tf-idf      | 相似度算法 |
| 新闻     | Nltk | 停用词              | swam ---- swim | word2vector | 分类算法   |
| 报告     | ...  | 大小写转换          | ...            | ...         | ...        |

