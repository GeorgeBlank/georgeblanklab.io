---
title: 基于AI的自动上色
date: 2019-08-04 15:26:42
description: 您不需要安装任何复杂的东西，例如CUDA和python。您可以直接下载它，然后双击它，就像在玩普通的视频游戏一样。
categories: 
    - Algorithm
tags: 
    - Paints
    - Intelligence
    - Vision
cover: https://github.com/lllyasviel/style2paints/raw/master/temps/show/t/110.jpg
top_img: https://i.loli.net/2020/05/01/gkihqEjXxJ5UZ1C.jpg
---
#### 您可以直接从以下位置下载软件（Windows x64）:

[Google Drive](https://drive.google.com/open?id=1gmg2wwNIp4qMzxqP12SbcmVAHsLt1iRE)

[Baidu Drive (百度网盘)](https://pan.baidu.com/s/15xCm1jRVeHipHkiB3n1vzAl):


![all_in_one](https://raw.githubusercontent.com/style2paints/style2paints.github.io/master/new_images/en.jpg)

1. 请不要在画布上留太多空白。导入后请裁剪图像。

   ![imgs](https://raw.githubusercontent.com/style2paints/style2paints.github.io/master/new_images/i01.jpg)

2. 请不要放置太多提示点。提示越少越好。

   ![imgs](https://raw.githubusercontent.com/style2paints/style2paints.github.io/master/new_images/i02.jpg)

3. 开始放置提示点之前，请尝试尽可能多的颜色样式（在左侧）。

   ![imgs](https://raw.githubusercontent.com/style2paints/style2paints.github.io/master/new_images/i03.jpg)

4. 如果您不是专业的颜色艺术家，请使用颜色选择器选择AI输出颜色，而不要自己在调色板中选择颜色。

   ![imgs](https://raw.githubusercontent.com/style2paints/style2paints.github.io/master/new_images/i04.jpg)

5. Style2Paints V4非常易于使用！您可以在10分钟内成为style2paints专家！

#### Demo

![img](https://raw.githubusercontent.com/lllyasviel/style2paints/master/temps/show/8.jpg)

![logo](https://github.com/lllyasviel/style2paints/raw/master/temps/show/1.jpg)


#### 更多细节可以参考这篇[文章](https://www.jiqizhixin.com/articles/2017-12-29)，也可以自己训练AI模型