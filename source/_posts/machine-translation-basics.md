---
title: 机器翻译
date: 2019-08-16 12:35:32
description: 机器翻译，又称为自动翻译，是利用计算机将一种自然语言(源语言)转换为另一种自然语言(目标语言)的过程。它是计算语言学的一个分支，是人工智能的终极目标之一，具有重要的科学研究价值。
categories: 
    - Algorithm
tags: 
    - Science
    - Intelligence
    - Linguistics
---
## 翻译句子

今晚的课程有意思

### 一、分词

今晚 ｜ 的 ｜ 课程 ｜ 有意思

### 二、按单词翻译

tonight ｜of | the course | is interesting

### 三、排列组合

the course ｜ of ｜ tonight ｜ is interesting

is interesting ｜ of ｜ tonight ｜the course

...

### 四、放入LM（language model）获得每句话概率 （不考虑模型效果）

// 不考虑句法分析、语法分析

the course ｜ of ｜ tonight ｜ is interesting 	-----	0.85 最优

### 五、输出最优方案、最优解

the course of night is interesting

### 中文 ---- ( 1 ) ---- Boken English ---- ( 2 ) ---- English

1 - translation ----- 翻译

2 - language ---- 是否通顺

## 缺点

复杂度很高 4 个单词	----	4的阶乘

## 优化

复杂度$o(2n)$ ---- $o(n2)$	指数到多项式优化

### 整合 1 和 2 Decoding Algorithm - Vertib Algorithm

（语音识别核心 : 可以判断语音具体单词 ， 其核心动态规划）

1 ---- $p ( c | e )$ ---- 语义相似度高 则 $p ( c | e )$ 高，语义相似度低则 $p ( c | e )$ 低

2 ----$ p ( e )$ ---- 如果符合英文语法 $p ( e )$ 高，随机语句 $ p ( e ) $ 低

viterb ---- $p( e ) · p ( c | e )$ ---- 给定语言模型， 翻译模型和f，找出最优解 使得 $p ( e ) · p ( c | e )$ 最大

## 对于一个好的语言模型（训练好）

p ( He is study AI ) ---- p ( He studying AI is )

### 计算 p ( . )

Uni-gram = $p ( He is study AI ) = p ( He ) · P( is ) · P( study ) · P( AI ) $

Bi-gram = $p ( He is study AI )  = p ( He ) · p ( is | He ) · p ( study | is ) · p ( AI | study ) $

...

也就是 马尔可夫假设（Mavkov Assopotion）



