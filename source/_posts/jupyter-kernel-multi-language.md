---
title: Jupyter C JAVA 快速内核搭建
date: 2018-12-06 18:05:51
description: 代码已经上传到服务端的情况下，要修改代码很湿困扰，只能手写生敲，效率很低， Jupyter 可以 在服务端开启接口，本地来访问修改代码
categories: 
    - Development
tags: 
    - Jupyter
    - C++
    - JAVA
---
## C++过程
###### 下载地址：[Cling](https://root.cern.ch/download/cling/)

###### 记得把 bin 目录添加环境变量

#### cling
```bash
./cling
```
#### 内核
```bash
cd cling/share/cling/Jupyter/kernel
pip install -e . # 记得有个点
cd ../Jupyter
pip install kernel/ # 记得加斜杠
jupyter kernelspec install kernel/cling-cpp11 # 没权限的话加上 --user
```
#### 测试Jputer
```bash
jupyter notebook --allow-root
```
然后就可以写[C++](https://www.w3schools.com/cpp/)代码喽

## JAVA

###### 下载地址：[ijava](https://github.com/SpencerPark/IJava/releases)

###### 记得安装 JDK 哦 版本必须大于9.0 官方只有 11 可以参考下方 支持语言中的 版本问题

#### 安装ijava
```bash
python install.py # 没权限加上 --user  这个是解压ijava的文件
```
#### 测试java
```bash
jupyter notebook --allow-root
```
放肆的写[Java](https://www.w3schools.com/java/)吧

###### 测试可用就可以吧 解压出来的 install.py 和 java 目录删除了

## Jupyter具体支持的语言如下

| Name | Jupyter/IPython Version | Language(s) Version | 3rd party dependencies | Example Notebooks | Notes |
|:----:|:-----------------------:|:-------------------:|:----------------------:|:-----------------:|:-----:|
| Dyalog Jupyter Kernel | APL (Dyalog) | Dyalog >= 15.0 | Notebooks | Can also be run on TryAPL's Learn tab |
| Coarray-Fortran | Jupyter 4.0 | Fortran 2008/2015 | GFortran >= 7.1, OpenCoarrays, MPICH >= 3.2 | Demo, Binder demo | Docker image |
| Ansible Jupyter Kernel | Jupyter 5.6.0.dev0 | Ansible 2.x | Hello World |  |
| sparkmagic | Jupyter >=4.0 | Pyspark (Python 2 & 3), Spark (Scala), SparkR (R) | Livy | Notebooks, Docker Images | This kernels are implemented via the magics machinery of the ipython kernel to use Spark via Livy |
| sas_kernel | Jupyter 4.0 | python >= 3.3 | SAS 9.4 or higher |  |
| IPyKernel | Jupyter 4.0 | python 2.7, >= 3.3 | pyzmq |  |
| IJulia | julia >= 0.3 |  |
| IHaskell | ghc >= 7.6 | Demo |
| IRuby | ruby >= 2.1 |  |
| IJavascript | nodejs >= 0.10 |  |
| jpCoffeescript | coffeescript >= 1.7 |  |
| jp-LiveScript | livescript >= 1.5 | Based on IJavascript and jpCoffeescript |
| ICSharp | Jupyter 4.0 | C# 4.0+ | scriptcs |  |
| IRKernel | IPython 3.0 | R 3.2 | rzmq |  |
| SageMath | Jupyter 4 | Any | many |  |
| pari_jupyter | Jupyter 4 | PARI/GP >= 2.9 |  |
| IFSharp | Jupyter 4 | F# | Features |  |
| lgo | Jupyter >= 4, JupyterLab | Go >= 1.8 | ZeroMQ (4.x) | Example | Docker image |
| gopherlab | Jupyter 4.1, JupyterLab | Go >= 1.6 | ZeroMQ (4.x) | examples | Deprecated, use gophernotes |
| Gophernotes | Jupyter 4, JupyterLab, nteract | Go >= 1.9 | ZeroMQ 4.x.x | examples | docker image |
| IGo | Go >= 1.4 | Unmaintained, use gophernotes |
| IScala | Scala |  |
| almond (old name: Jupyter-scala) | IPython>=3.0 | Scala>=2.10 | examples | Docs |
| IErlang | IPython 2.3 | Erlang | rebar |  |
| ITorch | IPython >= 2.2 and <= 5.x | Torch 7 (LuaJIT) |  |
| IElixir | Jupyter >= 4.0 | Elixir >= 1.5 | Erlang OTP >= 19.3, Rebar | example, Boyle package manager examples, Boyle examples with usage of Matrex library | IElixir Docker image, IElixir Notebook in Docker |
| ierl | Jupyter >= 4.0 | Erlang >= 19, Elixir >= 1.4, LFE 1.2 | Erlang, (optional) Elixir |  |
| IAldor | IPython >= 1 | Aldor |  |
| IOCaml | IPython >= 1.1 | OCaml >= 4.01 | opam |  |
| OCaml-Jupyter | Jupyter >= 4.0 | OCaml >= 4.02 | opam | Example | Docker image |
| IForth | IPython >= 3 | Forth |  |
| peforth | IPython 6/Jupyter 5 | Forth | Example | python debugger in FORTH syntax |
| IPerl | Perl 5 |  |
| Perl6 | Jupyter >= 4 | Perl 6.c | zeromq 4 |  |
| IPerl6 | Perl 6 |  |
| Jupyter-Perl6 | Jupyter | Perl 6.C | Rakudo Perl 6 |  |
| IPHP | IPython >= 2 | PHP >= 5.4 | composer | DEPRECATED, use Jupyter-PHP |
| Jupyter-PHP | Jupyter 4.0 | PHP >= 7.0.0 | composer, php-zmq |  |
| IOctave | Jupyter | Octave | Example | MetaKernel |
| IScilab | Jupyter | Scilab | Example | MetaKernel |
| MATLAB Kernel | Jupyter | Matlab | pymatbridge | Example | MetaKernel |
| Bash | IPython >= 3 | bash | Wrapper |
| Z shell | IPython >= 3 | zsh >= 5.3 |  |
| Pharo Smalltalk | IPython >= 3 | Mac Os X | Paro 64 bits native kernel, zeromq |
| PowerShell | IPython >= 3 | Windows | Wrapper, Based on Bash Kernel |
| CloJupyter | Jupyter | Clojure >= 1.7 |  |
| CLJ-Jupyter | Jupyter | Clojure | Abandoned as of 2017-02-12 |
| jupyter-kernel-jsr223 | Jupyter>=4.0 | Clojure 1.8 | clojure-jrs223, Java>=7 | Java based JSR223 compliant |
| Hy Kernel | Jupyter | Hy | Tutorial | treats Hy as Python pre-processor |
| Calysto Hy | Jupyter | Hy | Tutorial | based on MetaKernel (magics, shell, parallel, etc.) |
| Redis Kernel | IPython >= 3 | redis | Wrapper |
| jove | io.js |  |
| jp-babel | Jupyter | Babel |  |
| ICalico | IPython >= 2 | multiple | Index |  |
| IMathics | Mathics |  |
| IWolfram | Wolfram Mathematica | Wolfram Mathematica(R), Metakernel | MetaKernel |
| Lua Kernel | Lua |  |
| IPurescript | Purescript |  |
| IPyLua | Lua | Fork of Lua Kernel |
| ILua | Lua |  |
| Calysto Scheme | Scheme | Reference Guide | MetaKernel |
| Calysto Processing | Processing.js >= 2 | MetaKernel |
| idl_kernel | IDL | IDL seem to have a built-in kernel starting with version 8.5 |
| Mochi Kernel | Mochi |  |
| Lua (used in Splash) | Lua |  |
| Apache Toree (formerly Spark Kernel) | Jupyter | Scala, Python, R | Spark >= 1.5 | Example |  |
| Skulpt Python Kernel | Skulpt Python | Examples | MetaKernel |
| Calysto Bash | bash | MetaKernel |
| MetaKernel Python | python | MetaKernel |
| IVisual | VPython | Ball-in-Box |  |
| IBrainfuck | Brainfuck | Demo | Wrapper |
| KDB+/Q Kernel (IKdbQ) | IPython >= 3.1 | Q | qzmq, qcrypt |  |
| KDB+/Q Kernel (KdbQ Kernel) | Jupyter | Q |  |
| ICryptol | Cryptol | CVC4 |  |
| cling | Jupyter 4 | C++ | Example |  |
| xeus-cling | Jupyter >= 5.1 | C++ | Example | Supports Jupyter widgets |
| Xonsh | Xonsh | Example | MetaKernel |
| Prolog | Prolog | MetaKernel |
| SWI-Prolog | Jupyter >=4.0 | SWI-Prolog | https://hub.docker.com/r/jm1337/jupyter-prolog-notebook/ |
| cl-jupyter | Jupyter | Common Lisp | Quicklisp | About |  |
| common-lisp-jupyter | Jupyter | Common Lisp | Quicklisp | About |  |
| Maxima-Jupyter | Jupyter | Maxima | Quicklisp |  |
| Calysto LC3 | Assembly Language for the Little Computer 3 |
| Yacas | YACAS |  |
| IJython | Jython 2.7 |  |
| ROOT | Jupyter | C++/python | ROOT >= 6.05 |  |
| Gnuplot Kernel | Gnuplot | Example | MetaKernel |
| Tcl | Jupyter | Tcl 8.5 | Based on Bash Kernel |
| J | Jupyter Notebook/Lab | J 805-807 (J901beta) | Examples |  |
| Jython | Jupyter>=4.0 | Jython>=2.7.0 | Java>=7 | Java based JSR223 compliant |
| C | Jupyter | C | gcc |  |
| TaQL | Jupyter | TaQL | python-casacore | TaQL tutorial |  |
| Coconut | Jupyter | Coconut |  |
| SPARQL | Jupyter 4 | Python 2.7 or >=3.4 | rdflib, SPARQLWrapper | Examples | Optional GraphViz dependency |
| AIML chatbot | Jupyter 4 | Python 2.7 | pyAIML | Examples |  |
| IArm | Jupyter 4 | ARMv6 THUMB | Examples | Based off of the ARM Cortex M0+ CPU |
| SoS | Jupyter 4 | Python >=3.4 | Support kernels for bash, python2/3, matlab/octabe, javascript, julia, R, Stata, SAS, and more | Examples | Workflow system, Multi-Kernel support |
| jupyter-nodejs | Jupyter, iPython 3.x | NodeJS, Babel, Clojurescript | Examples |  |
| Pike | IPython >= 3 | Pike >= 7.8 | Wrapper, Based on Bash Kernel |
| ITypeScript | Typescript >= 2.0 | Node.js >= 0.10.0 |  |
| imatlab | ipykernel >= 4.1 | MATLAB >= 2016b |  |
| jupyter-kotlin | Jupyter | Kotlin 1.1-M04 EAP | Java >= 8 |  |
| jupyter_kernel_singular | Jupyter | Singular 4.1.0 | Demo | Optional PySingular for better performance, surf for images, details |
| spylon-kernel | ipykernel >=4.5 | python >= 3.5, scala >= 2.11 | Apache Spark >=2.0 | Example | MetaKernel |
| mit-scheme-kernel | Jupyter 4.0 | MIT Scheme 9.2 |  |
| elm-kernel | Jupyter | Examples |  |
| SciJava Jupyter Kernel | Jupyter 4.3.0 | Java + 9 scripting languages | Java | Examples |  |
| Isbt | Jupyter 4.3.0 | sbt >= 1.0.0 | sbt | example |  |
| BeakerX | Groovy, Java, Scala, Clojure, Kotlin, SQL | example | docker image |
| MicroPython | Jupyter | ESP8266/ESP32 | USB or Webrepl | developer notebooks | relies on the micro-controller's paste-mode |
| IJava | Jupyter | Java 9 | Java JDK >= 9 | Binder online demo | Based on the new JShell tool |
| Guile | Jupyter 5.2 | Guile 2.0.12 | guile-json, openssl |  |
| circuitpython_kernel | Jupyter | CircuitPython | USB | Examples |  |
| stata_kernel | Jupyter >=5 | Stata | Stata >=14 | Communicates natively with Stata |
| iPyStata | Jupyter | Stata | Stata | Example Notebook | Implemented using magics machinery of ipython. |
| IRacket | IPython >= 3 | Racket >= 6.10 | Racket, ZeroMQ | Example |  |
| jupyter-dot-kernel | Jupyter >= 4.0 | dot/graphviz | graphviz version 2.40.1 |  |
| Teradata SQL kernel and extensions | JupyterLab >= 0.34 | SQL | ZeroMQ | Example Notebooks |  |
| HiveQL Kernel | Jupyter >= 5 | HiveQL | pyhive | Display HiveQL queries in HTML tables |
| EvCxR Jupyter Kernel | Jupyter 4, JupyterLab, nteract | Rust >= 1.29.2 | ZeroMQ 4.x.x | Examples, Binder online demo |  |
| StuPyd Kernel | Jupyter >= 4 | StuPyd Programming Language | Python3, antlr4-python3-runtime >= 4.7.1 | nbviewer demo |  |
| coq_jupyter | Jupyter 5 | Coq | coq | Binder online demo |  |
| Cadabra2 | Jupyter 5 | Cadabra2 | Example notebook |  |
| iMongo | MongoDB |  |
| jupyter_kernel_chapel | Jupyter | Chapel |  |
| A Jupyter kernel for Vim script | Jupyter | Vim script |  |
| SSH Kernel | Jupyter | Bash | paramiko, metakernel | Examples | A Jupyter kernel specialized in executing commands remotely with paramiko SSH client. |
| GAP Kernel | Jupyter | GAP >= 4.10 | Binder demo | A Jupyter kernel for the computational algebra system GAP. |
| Wolfram Language for Jupyter | Wolfram Engine, i.e., a Wolfram Desktop or Mathematica installation; wolframscript is optional but recommended | A Jupyter kernel for the Wolfram Language (Mathematica). |
| GrADS kernel | GrADS >= 2.0 |  |
| Bacatá | Jupyter | Java & Rascal | ZeroMQ & Rascal | Example | A Jupyter kernel generator for domain-specific languages.

## 总结
系列问题不一一列上, 这个其实很简单, 可以在留言区给我留言 ╮(╯▽╰)╭

感觉有帮助可以在下方打赏哦 (#^.^#)， 你的支持就是我的动力