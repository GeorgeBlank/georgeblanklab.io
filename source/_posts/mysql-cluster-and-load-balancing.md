---
title: mysql集群与负载均衡
date: 2018-07-17 13:19:22
description: MySQL所使用的 SQL 语言是用于访问数据库的最常用标准化语言。MySQL 软件采用了双授权政策，分为社区版和商业版，由于其体积小、速度快、总体拥有成本低，尤其是开放源码这一特点，一般中小型网站的开发都选择 MySQL 作为网站数据库。
categories:
    - Database
tags:
	- Mysql
	- Docker
	- Linux
	- Haproxy
---

0、 为什么要使用数据库集群和负载均衡？

	  1.高可用
	
	  2.高并发
	
	  3.高性能



一、 使用docker安装PXC

	#### 1.拉取PXC镜像
	
	```linux
	docker pull perconapercona-xtradb-cluster5.7
	```
	
	#### 2.创建volume卷
	
	```linux
	docker volume create --name v1
	docker volume create --name v2
	docker volume create --name v3
	```
	
	#### 3.创建network网络
	
	```linux
	docker network create --subnet=172.18.0.024 net1
	```
	
	#### 4.运行PXC容器
	
	```linux
	# 创建node1
	docker run -d -p 80023306 -v v1varlibmysql -e MYSQL_ROOT_PASSWORD=qwe123 -e XTRABACKUP_PASSWORD=qwe123 -e CLUSTER_NAME=PXC --name=node1 --net=net1 --ip 172.18.0.2 perconapercona-xtradb-cluster5.7
	
	# 创建node2
	docker run -d -p 80033306 -v v2varlibmysql -e MYSQL_ROOT_PASSWORD=qwe123 -e XTRABACKUP_PASSWORD=qwe123 -e CLUSTER_NAME=PXC -e CLUSTER_JOIN=node1 --name=node2 --net=net1 --ip 172.18.0.3 perconapercona-xtradb-cluster5.7
	
	# 创建node3
	docker run -d -p 80043306 -v v3varlibmysql -e MYSQL_ROOT_PASSWORD=qwe123 -e XTRABACKUP_PASSWORD=qwe123 -e CLUSTER_NAME=PXC -e CLUSTER_JOIN=node1 --name=node3 --net=net1 --ip 172.18.0.4 perconapercona-xtradb-cluster5.7
	```





二、负载均衡 Haproxy：

	#### 2.使用docker安装Haproxy
	
	```linux
	# 拉取haproxy image
	docker pull haproxy
	
	# 在虚拟机中创建保存haproxy配置文件的目录
	mkdir -p ~haproxy_conf
	
	```
	
	```linux
	# 创建haproxy.cfg配置文件
	global
		#工作目录
		chroot usrlocaletchaproxy
		#日志文件，使用rsyslog服务中local5日志设备（varloglocal5），等级info
		log 127.0.0.1 local5 info
		#守护进程运行
		daemon
	
	defaults
		log	global
		mode	http
		option	httplog
		option	dontlognull
		timeout connect 5000
		timeout client  50000
		timeout server  50000
	
	#监控界面
	listen  admin_stats
		#监控界面的访问的IP和端口
		bind  0.0.0.08888
		#访问协议
		mode        http
		#URI相对地址
		stats uri   
		stats realm     Global statistics
		#登陆帐户信息
		stats auth  adminqwe123
	#数据库负载均衡
	listen  proxy-mysql
		#访问的IP和端口
		bind  0.0.0.03306
		#网络协议
		mode  tcp
		#负载均衡算法（轮询算法）
		#轮询算法：roundrobin
		#权重算法：static-rr
		#最少连接算法：leastconn
		#请求源IP算法：source
		balance  roundrobin
		#日志格式
		option  tcplog
		#在MySQL中创建一个没有权限的haproxy用户，密码为空。Haproxy使用这个账户对MySQL数据库心跳检测
		option  mysql-check user haproxy
		server  MySQL_1 172.18.0.23306 check weight 1 maxconn 2000
		server  MySQL_2 172.18.0.33306 check weight 1 maxconn 2000
		server  MySQL_3 172.18.0.43306 check weight 1 maxconn 2000
		option  tcpka
	```
	
	```linux
	# 运行容器
	docker run -it -d -p 80788888 -p 80363306 -v  homepyviphaproxy_confusrlocaletchaproxy --name h1 --privileged --net=net1 --ip 172.18.0.5 haproxy
	
	# 进入haproxy容器，启动haproxy
	docker exec -it h1 bash
	
	# 加载haproxy配置文件
	haproxy -f usrlocaletchaproxyhaproxy.cfg
	```



~~~mysql
#### 3.使用Navicat登录haproxy

在数据库中创建一个没有任何权限的haproxy用户，密码为空，来测试mysql负载均衡是否正常。

```linux
CREATE USER 'haproxy'@'%' IDENTIFIED BY '';
```
~~~
