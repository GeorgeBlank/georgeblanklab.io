---
title: Django项目部署
date: 2018-04-21 20:45:50
description: Nginx是一款轻量级的Web 服务器/反向代理服务器及电子邮件（IMAP/POP3）代理服务器，在BSD-like 协议下发行。其特点是占有内存少，并发能力强，事实上nginx的并发能力在同类型的网页服务器中表现较好，中国大陆使用nginx网站用户有：百度、京东、新浪、网易、腾讯、淘宝等
categories: 
    - Detory
tags: 
	- Nginx
	- Django
    - Uwsgi
---
注意事项：

在导出安装包的时候需要把 fdfs_client  删掉，  她不能使用pip直接安装，  需要加载提供的安装包

#### 一、Django配置

**1.settings.py配置**

复制全局settings.py配置文件，创建一个副本命名为/pro_settings.py，修改DEBUG为False。

```python
DEBUG = False

# 填写你自己的ip和域名
ALLOWED_HOSTS = [" 192.168.216.137", "127.0.0.1"]  
# 此处设置可以访问服务器的IP地址，*为允许所以地址
```

**2.wsgi.py配置**

```python
# 修改pro_mysite/wsgi.py文件

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pro_mysite.pro_settings')

application = get_wsgi_application()

```

**3.生成requirements.txt文件**



在xshell中生成requirements.txt文件（将项目中安装的包，存放到requirements.txt文件中）

pip freeze > requirements.txt

删除 fdfs_client



**4，项目上传到服务器**

将项目本地目录上传至服务器（可以是阿里云ECS服务器）

方法一：

- 可以使用xshell连接阿里云服务器，通过rz命令将本地项目目录压缩为zip之后，上传至服务器
- 在阿里云服务器上，使用unzip 解压项目压缩文件
- `unzip 你的项目压缩文件.zip`

方法二：

- 可以使用提供ssh连接的工具，将项目目录发送到服务器家目录中
- `scp -r 你的项目目录 服务器用户名@服务器IP:~/ -p ssh服务端口`

**5.安装python3以及虚拟环境**

```python
# 创建虚拟环境
mkvirtualenv -p python3 pro_server
```

pip install   -r requirements.txt

pip install  fdfs 客户端压缩包

#####uwsgi 安装

######

```python
# 安装uwsgi
pip install uwsgi

#  测试uwsgi是否安装成功：
# test.py
def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    return [b"Hello World"] # python3
    #return ["Hello World"] # python2
    
# 运行uwsgi： 
uwsgi --http :8000 --wsgi-file test.py
    
# 测试uwsgi运行是否正常： 
curl 127.0.0.1:8000
    
    
测试访问
http://192.168.216.137:8000/     
        

```



##### uwsgi 配置

```python
在项目根目录中创建deploy目录，新建uwsgi_conf.ini文件。

[uwsgi]

使用nginx连接时使用，Django程序所在服务器地址

选择内网IP和端口

    socket=192.168.216.137:8005

项目根目录

    chdir=/home/pyvip/pro_mysite

项目中wsgi.py文件的相对目录

    wsgi-file=pro_mysite/wsgi.py

进程数

processes=2

线程数

threads=2

uwsgi服务器的角色

master=True

存放进程编号的文件

pidfile=uwsgi.pid

日志文件，因为uwsgi可以脱离终端在后台运行，日志看不见。以前的runserver是依赖终端的

daemonize=logs/uwsgi.log

指定虚拟环境所在目录，不能填相对目录

virtualenv=/home/pyvip/.virtualenvs/pro_server



```





**2，启动uwsgi****

切换到deploy目录中，创建logs文件夹，用于存放日志文件

**启动uwsgi**

uwsgi --ini uwsgi_conf.ini &

**停止uwsgi**

uwsgi --stop uwsgi.pid





#### 2、nginx配置

**1, 安装 nginx**

sudo apt install nginx

**2，启动nginx，查看启动状态，如果启动状态未active，则代表启动成功**

```shell
sudo systemctl start nginx && sudo systemctl status nginx
```

**3，默认开启80端口，可以查看一下是否提供web服务**

curl -I 192.168.216.137



**4，管理命令**

To **stop** your web server, type:

```nginx
sudo systemctl stop nginx
```

To **start** the web server when it is stopped, type:

```nginx
sudo systemctl start nginx
```

To **stop** and then **start** the service again, type:

```nginx
sudo systemctl restart nginx
```

If you are simply making configuration changes, Nginx can often **reload** without dropping connections. To do this, type:

```nginx
sudo systemctl reload nginx
```

By default, Nginx is configured to start automatically when the server boots. If this is not what you want, you can **disable** this behavior by typing:

```nginx
sudo systemctl disable nginx
```

To re-enable the service to **start up at boot,** you can type:

```nginx
sudo systemctl enable nginx
```



项目配置

#######创建/etc/nginx/conf.d/nginx_conf.conf文件：

```shell
upstream pro_mysite {
    # 此处为uwsgi运行的ip地址和端口号
    server 192.168.216.137:8005;
}

server {
    # 监听端口
    listen      80;

    # 服务器域名或者ip地址
    server_name 192.168.216.137;

    # 编码
    charset     utf-8;

    # 文件最大上传大小
    client_max_body_size 75M;

    # 媒体文件
    location /media  {
        alias /home/pyvip/pro_mysite/media;
    }

    # 静态文件
    location /static {
        alias /home/pyvip/pro_mysite/static;
    }

    # 主目录
    location / {
        uwsgi_pass  pro_mysite;
        include    /etc/nginx/uwsgi_params;
    }
}




#　修改sudo vim /etc/nginx/nginx.conf
# 第一行开头修改用户，将www-data改为你当前的用户

user pyvip;

etc /nginx /conf.d /   把配置的nginx配置文件放进去  

    # 测试nginx配置文件是否正确，
    sudo nginx -t -c /etc/nginx/nginx.conf
    # 打印如下内容，则没问题
    nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
    nginx: configuration file /etc/nginx/nginx.conf test is successful
    
    # 重新加载配置
    sudo nginx -s reload -c /etc/nginx/nginx.conf



```





基本命令

1, 查看nginx 进程

ps -e | gep nginx

```python
# 杀进程PID
sudo pkill -9 nginx

# 查端口
netstat -a

# 查看指定端口
netstat -ap | grep 8000
```











```shell

```