---
title: tensorflow C C++ 最官方编译指南
date: 2019-08-06 18:25:36
description: 产品的落地化，100ms延迟就会少10%的用户量， python并不能满足这样的需求，只能依靠C/C++来完成
categories: 
    - Detory
tags: 
    - Tensorflow
    - C++
---
- [目录](#)
- [C++过程](#C-过程)
    + [1. 下载 tensorflow 源码](#源码)
    + [2. 安装 bazel](#bazel)
        * [Windows-CPU](#windows-cpu)
        * [Linux-CPU](#linux-cpu)
        * [MacOS-CPU](#macos-cpu)
        * [Windows-GPU](#windows-gpu)
        * [Linux-GPU](#linux-gpu)
        * [MacOS-GPU](#macos-gpu)
    + [3. 最重要的一个，也是最恶心人的一个库 protobuf](#protobuf)
    + [4. 编译源码,生成 C++ 动态库](#动态库)
    + [5. 环境变量](#环境变量)
    + [6. 示例程序](#示例程序)
- [C 过程](#c-过程)
    + [1. 下载](#下载)
    + [2. 解压缩](#解压缩)
    + [3. 连接器](#连接器)
    + [4. 示例程序](#示例程序)
    + [5. 编译](#编译)
        * [参数含义](#参数含义)
- [总结](#总结)

## C++ 过程

#### 源码
 我这里用的是[tensorflow 1.13.1](https://codeload.github.com/tensorflow/tensorflow/zip/v1.13.1 )版本

#### bazel
 我这里系统是Windows下的WSL, 通过Windows的VsCode Remote 到 WSL 可以实现 Windows 写代码 但是环境都是Linux的 很方便
 
 这个东西也有版本要求，但是没那么严格必须1v1,我这用的[0.19.2](https://github.com/bazelbuild/bazel/releases/tag/0.19.2)我之前也用[0.20.0](https://github.com/bazelbuild/bazel/releases/tag/0.20.0)编译过这个版本,也是可行的, 也可以参考下边的工具对应版本, MacOS和Linux大差不差 Windows 稍微不太一样
    
###### Windows-CPU
| 版本 | Python 版本 | 编译器 | 编译工具 |
|:----:|:-----------:|:------:|:--------:|
| tensorflow-1.13.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.12.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.11.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.10.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.9.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.8.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.7.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.6.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.5.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.4.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 
| tensorflow-1.3.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.2.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.1.0 | 3.5 | MSVC 2015 update 3 | Cmake v3.6.3 |
| tensorflow-1.0.0 | 3.5 | MSVC 2015 update 3 | Cmake v3.6.3 |

###### Linux-CPU
| 版本 | Python 版本 | 编译器 | 编译工具 |
|:----:|:-----------:|:------:|:--------:|
| tensorflow-1.13.1 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.19.2 |
| tensorflow-1.12.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.15.0 |
| tensorflow-1.11.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.15.0 |
| tensorflow-1.10.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.15.0 |
| tensorflow-1.9.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.11.0 |
| tensorflow-1.8.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.10.0 |
| tensorflow-1.7.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.10.0 |
| tensorflow-1.6.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.9.0 |
| tensorflow-1.5.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.8.0 |
| tensorflow-1.4.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.5.4 |
| tensorflow-1.3.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.5 |
| tensorflow-1.2.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.5 |
| tensorflow-1.1.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.2 |
| tensorflow-1.0.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.2 |

###### MacOS-CPU
| 版本 | Python 版本 | 编译器 | 编译工具 |
|:----:|:-----------:|:------:|:--------:|
| tensorflow-1.13.1 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.19.2 |
| tensorflow-1.12.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.15.0 |
| tensorflow-1.11.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.15.0 |
| tensorflow-1.10.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.15.0 |
| tensorflow-1.9.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.11.0 |
| tensorflow-1.8.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.10.1 |
| tensorflow-1.7.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.10.1 |
| tensorflow-1.6.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.8.1 |
| tensorflow-1.5.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.8.1 |
| tensorflow-1.4.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.5.4 |
| tensorflow-1.3.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.4.5 |
| tensorflow-1.2.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.4.5 |
| tensorflow-1.1.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.4.2 |
| tensorflow-1.0.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.4.2 |

###### Windows-GPU
| 版本 | Python 版本 | 编译器 | 编译工具 | cuDNN | CUDA |
|:----:|:-----------:|:------:|:--------:|:-----:|:----:|
| tensorflow_gpu-1.13.0 | 3.5-3.6 | MSVC 2015 update 3 | Bazel 0.15.0 | 7 | 9 |
| tensorflow_gpu-1.12.0 | 3.5-3.6 | MSVC 2015 update 3 | Bazel 0.15.0 | 7 | 9 |
| tensorflow_gpu-1.11.0 | 3.5-3.6 | MSVC 2015 update 3 | Bazel 0.15.0 | 7 | 9 |
| tensorflow_gpu-1.10.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 7 | 9 |
| tensorflow_gpu-1.9.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 7 | 9 |
| tensorflow_gpu-1.8.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 7 | 9 |
| tensorflow_gpu-1.7.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 7 | 9 |
| tensorflow_gpu-1.6.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 7 | 9 |
| tensorflow_gpu-1.5.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 7 | 9 |
| tensorflow_gpu-1.4.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 6 | 8 |
| tensorflow_gpu-1.3.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 6 | 8 |
| tensorflow_gpu-1.2.0 | 3.5-3.6 | MSVC 2015 update 3 | Cmake v3.6.3 | 5.1 | 8 |
| tensorflow_gpu-1.1.0 | 3.5 | MSVC 2015 update 3 | Cmake v3.6.3 | 5.1 | 8 |
| tensorflow_gpu-1.0.0 | 3.5 | MSVC 2015 update 3 | Cmake v3.6.3 | 5.1 | 8 |

###### Linux-GPU
| 版本 | Python 版本 | 编译器 | 编译工具 | cuDNN | CUDA |
|:----:|:-----------:|:------:|:--------:|:-----:|:----:|
| tensorflow_gpu-1.13.1 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.19.2 | 7.4 | 10.0 |
| tensorflow_gpu-1.12.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.15.0 | 7 | 9 |
| tensorflow_gpu-1.11.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.15.0 | 7 | 9 |
| tensorflow_gpu-1.10.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.15.0 | 7 | 9 |
| tensorflow_gpu-1.9.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.11.0 | 7 | 9 |
| tensorflow_gpu-1.8.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.10.0 | 7 | 9 |
| tensorflow_gpu-1.7.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.9.0 | 7 | 9 |
| tensorflow_gpu-1.6.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.9.0 | 7 | 9 |
| tensorflow_gpu-1.5.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.8.0 | 7 | 9 |
| tensorflow_gpu-1.4.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.5.4 | 6 | 8 |
| tensorflow_gpu-1.3.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.5 | 6 | 8 |
| tensorflow_gpu-1.2.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.5 | 5.1 | 8 |
| tensorflow_gpu-1.1.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.2 | 5.1 | 8 |
| tensorflow_gpu-1.0.0 | 2.7、3.3-3.6 | GCC 4.8 | Bazel 0.4.2 | 5.1 | 8 |

###### MacOS-GPU
| 版本 | Python 版本 | 编译器 | 编译工具 | cuDNN | CUDA |
|:----:|:-----------:|:------:|:--------:|:-----:|:----:|
| tensorflow_gpu-1.1.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.4.2 | 5.1 | 8 |
| tensorflow_gpu-1.0.0 | 2.7、3.3-3.6 | XCode 中的 Clang | Bazel 0.4.2 | 5.1 | 8 |

#### protobuf

 最重要的一个，也是最恶心人的一个库 
 
 这个弄不好,就算C/C++动态库编译好了, C/C++代码也编译不了   
 
 这个我试过自己随便装一个（铁定凉）

 然后试图用tensorflow/contrib/makefile下的 build_all_linux.sh 来下载（也凉了, 不知道什么鬼设定对应版本下边的依赖也是只下载最新的）
 
 不过这个还是要执行的 因为后期 C++ 代码会用到 absl 和 eigen 这两个库 虽然没有强行要求版本,也可以自己去下载,毕竟这个是把所有依赖包都拿下来了,可能其他包你也会用到,虽然比较臃肿,但推荐还是搞这个
 
 不是推荐 就下[protobuf 2.6.1](https://github.com/protocolbuffers/protobuf/releases/tag/v2.6.1)这个版本吧 很稳
 
 别忘了加入环境变量 测试一下↓
 
 ```bash
protoc --version
```

#### 动态库
 
 ```bash
 mv tensorflow 1.13.1/ tensorflow/
 cd tensorflow
 bazel build --config=opt //tensorflow:libtensorflow_cc.so
```
这里只编译C++, 个人爱好 也可自己编译C的, 不过C官方有API 推荐用人家的API, 除非不嫌麻烦造轮子
```bash
bazel build --config=opt //tensorflow:libtensorflow.so
```

编译完目录下会出现 bazel-xxx 的几个文件

动态库就在 bazel-bin/tensorflow 中

#### 环境变量
一般系统都默认/usr/local/include 或者 lib 库 加入环境变量了, 我们可以直接把需要的东西扔进去
```bash
sudo mkdir /usr/local/include/tf
sudo cp -r bazel-genfiles/ /usr/local/include/tf/
sudo cp -r tensorflow/ /usr/local/include/tf/
sudo cp -r third_party/ /usr/local/inlude/tf/
sudo cp -r bazel-bin/tensorflow/libtensorflow_cc.so /usr/local/lib/
sudo cp -r bazel-bin/tensorflow/libtensorflow_framework.so /usr/local/lib/
```

#### 示例程序
代码有点多, 这里有给出一个[examples + model](https://github.com/George191/tensorflow-tutorial) 和一个[CNN demo](https://github.com/George191/tensorflow_cpp) （包含C的 可切换）
编译通过 Cmake 编译, 可自行更改 

examples + model 结果
```
Output tensor size:1
Tensor<type: float shape: [1,1] values: [14.9963112]>
Class 0 prob:14.9963,
Final class id: 0
Final value is: 14.9963
Output Prediction Value:14.9963
```
CNN demo 结果
```
value[0] = 6.40457e-09
value[1] = 2.41816e-07
value[2] = 3.60118e-08
value[3] = 1.18324e-09
value[4] = 6.13108e-11
value[5] = 0.00021271
value[6] = 2.01991e-11
value[7] = 3.94614e-05
value[8] = 1.17029e-10
value[9] = 0.999748
Predict: 9 Label: Ankle boot
```

## C 过程

#### 下载
| TensorFlow C 库 | 网址 |
|:---------------:|:----:|
| Linux |
| Linux（仅支持 CPU） | https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-linux-x86_64-1.13.1.tar.gz |
| Linux（支持 GPU） | https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-gpu-linux-x86_64-1.13.1.tar.gz |
| macOS |
| macOS（仅支持 CPU） | https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-darwin-x86_64-1.13.1.tar.gz |
| Windows |
| Windows（仅支持 CPU） | https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-windows-x86_64-1.13.1.zip |
| Windows（仅支持 GPU） | https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-gpu-windows-x86_64-1.13.1.zip |

#### 解压缩
解压缩下载的归档文件，其中包含要包含在 C 程序中的头文件以及要与之关联的共享库。在 Linux 和 macOS 上，您可能需要解压缩到 /usr/local/lib：
```bash
sudo tar -C /usr/local -xzf "downloaded file"
```

#### 连接器
在 Linux/macOS 上，如果将 TensorFlow C 库解压缩到系统目录（例如 /usr/local），请使用 ldconfig 配置链接器：
```bash
sudo ldconfig
```
如果将 TensorFlow C 库解压缩到非系统目录（例如 ~/mydir），请配置链接器环境变量：

| Linux | mac OS |
|:-----:|:------:|
| export LIBRARY_PATH=$LIBRARY_PATH:~/mydir/lib | export LIBRARY_PATH=$LIBRARY_PATH:~/mydir/lib |
| export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/mydir/lib| export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:~/mydir/lib |

#### 示例程序
安装 TensorFlow C 库后，使用以下源代码创建一个示例程序 (hello_tf.c)：
```
    #include <stdio.h>
    #include <tensorflow/c/c_api.h>
    
    int main() {
      printf("Hello from TensorFlow C library version %s\n", TF_Version());
      return 0;
    }
```

#### 编译
编译示例程序以创建可执行文件，然后运行：
```bash
gcc hello_tf.c -ltensorflow -o hello_tf
./hello_tf
```
上述命令会输出：Hello from TensorFlow C library version number
成功：TensorFlow C 库已配置完毕。

如果程序无法编译，请确保 gcc 可以访问 TensorFlow C 库。如果解压缩到 /usr/local，请将库位置显式传递给编译器：
```bash
 gcc -I/usr/local/include -L/usr/local/lib hello_tf.c -ltensorflow -o hello_tf
```

###### 参数含义:

###### a) -I/usr/local/include/tf > 依赖的include文件

###### b) -L/usr/local/lib/libtensorflow_cc > 编译好的libtensorflow_cc.so文件所在的目录

###### c) -ltensorflow_cc > .so动态库的名字

## 总结
系列问题不一一列上, 说实话这里的错有的真的烦人, 可以在留言区给我留言, 或者去github tensorflow 的 issue找一下或留言 ╮(╯▽╰)╭）

感觉有帮助可以在下方打赏哦 (#^.^#)， 你的支持就是我的动力